

def by_two_thirds(counter, last, margin):
	""""Calculate the limit of 1 + 2/3 + (2/3 * 2/3)..."""
	if last == None:
		print "1"
		by_two_thirds(counter, counter, 2.0/3)
	else:
		counter += margin
		if abs(last - counter) < 0.0000000001:
			print counter
		else:
			print counter
			by_two_thirds(counter + margin, counter, margin * 2.0 / 3)

by_two_thirds(1, None, None)